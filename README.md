DISCLAIMER: Due to mental and physical health issues, my work capacity has been significantly reduced. As a consequence, the messaging app is relatively basic and insecure.

shoutout to caffeine and paracetamol.

Okay on to the actual stuff:

-DESIGN CONSIDERATIONS: In my limited state, the only real consideration was making something that works.
Due to that, there really hasn't been any improvements or refactoring to the existing code.

-FEATURES: The features of the messaging system are:
-The ability to log in or create an account.
-The ability to send messages.
-The ability to see messages to or from you.
-NOTE: Due to time constraints, there is no separate account creation page. If you try to log in with a username not in the database, it will instead create an account. Very bad system, but better than nothing.
-NOTE 2: For some godforsaken reason the logout method worked with an auto refresh and then randomly didn't anymore. So you sadly have to manually hit refresh after pressing log out as I have no time for a fix

-FEATURES THAT SHOULD HAVE BEEN:
-Session data is not stored due to time constraint, it should have been
-Input should be run through a parser method to prevent SQL injections.
-Messages are taken straight from the database and displayed as arrays of values, should be a function to reformat them to something more clear and user friendly

-HOW TO TEST: 
1) Run the program with flask run
2) Connect to http://localhost:5000/
3) Create an account
4) log out and log back in to confirm it works
5) Send a message to 'devil'(default value in To: field), and a message to anyone else.
6) Log out, log in as 'devil' with the password 'hell', confirm that the first message appears when you hit 'show all' and that the second message doesn't

-TECHNICAL DETAILS:
-Usernames and passwords are stored in the database, passwords are stored in an encoded form for security.
-database tables for messages with sender, recipient, timestamp, content and id.

-SECURITY FLAWS:
-Realistically there is not much of a threat as there is nothing of value on the application. If we were to imagine someone somehow wanting to attack the application, it is quite vulnerable.
-The easiest attack vector is probably SQL injection, as there is nothing preventing an attacker from inserting whatever they want into a query. Essentially, the applications database can be completely extracted or destroyed with ease. This should be prevented by properly parsing user input to make sure there are no shenanigans going on.
-The cryptography is rather weak too. The passwords are encoded with base64.b64encode(), so they can be aquired with b64decode(). Slightly better than plaintext, but not by much.
-There is no logging, so if an attack was to take place we would not know.
